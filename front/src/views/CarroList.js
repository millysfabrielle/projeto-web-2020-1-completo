import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import CustomModal from '../components/CustomModal';

import { connect } from "react-redux";
import { listarCarro, excluirCarro  } from "../redux/actions/CarroActions";
import { carroListSelector } from "../redux/selectors/CarroSelectors";

class CarroList extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            carroId: null,
            show: false,
            modalInfo: {
                tittle: '',
                body: '',
                btnSave: '',
                btnCancel: ''

            }
        };

        // chamar o método do ACTIONS
        this.props.listarCarro()
        .then( () => {
        });

    }

    handleClose = () => { this.setState({ show: false }); }

    handleShow = (carro) =>  {
        this.setState({
            show: true,
            carroId: carro.id,
            modalInfo: {
                tittle: 'Confirmação de exclusão!',
                body: 'Deseja excluir o carro de placa: '+ carro.placa,
                btnSave: 'Excluir',
                btnCancel: 'Cancelar'

            }
        });
    }

    handleBtnExcluir(id)    {
        this.props.excluirCarro(id)
        .then( () => {
            this.handleClose();
        });


    }

    render() {
        return (
            <div>
                <Link to="/carro/form" className="btn btn-primary" >Novo</Link>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Marca</th>
                            <th scope="col">Modelo</th>
                            <th scope="col">Cor</th>
                            <th scope="col">Placa</th>
                            <th scope="col">Ano Fab.</th>
                            <th scope="col">Operações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.carroLista.map( carro => 

                                <tr key={carro.id}>
                                    <td>{carro.marca}</td>
                                    <td>{carro.modelo}</td>
                                    <td>{carro.cor}</td>
                                    <td>{carro.placa}</td>
                                    <td>{carro.anoFab}</td>
                                    <td>
                                        <Link to={`/carro/form/${carro.id}`} className="btn btn-primary m-10">Alterar</Link>

                                        <button type="button" className="btn btn-primary" onClick={() => this.handleShow(carro)} >Excluir</button>

                                    </td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>

                <CustomModal ref={this.wrapper}
                    show={this.state.show} 
                    handleClose={() => {this.handleClose()}} 
                    handleBtnExcluir={() => this.handleBtnExcluir(this.state.carroId)} 
                    modalInfo={this.state.modalInfo} 
                />

            </div>
        );
    }

}

// selector, { métodos do action}, 
export default connect(carroListSelector, { listarCarro, excluirCarro })(CarroList);