import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage, } from 'formik';

import { connect } from "react-redux";
import { salvarCarro, buscarCarro } from "../redux/actions/CarroActions";
import { carroSelector } from "../redux/selectors/CarroSelectors";


var initialValues = {
    anoFab: "",
    placa: "",
    marca: "",
    modelo: "",
    cor: ""
}


class CarroForm extends Component {

    
    constructor(props) {
        super(props);
        const { id } = this.props.match.params;
        if(id){
            this.props.buscarCarro(id);
        }
    }

    handleSubmit(values, {setSubmitting}, props) {

        this.props.salvarCarro(values);

        // habilitando o btn de salvar
        setSubmitting(false);

        // voltar para página anterior
        props.history.push('/carro/list');

    }

    render() {
        return (
            <div>

                <Formik 
                    initialValues = { this.props.match.params.id ? this.props.carro : initialValues }
                    enableReinitialize
                    onSubmit={ (values, actions) => this.handleSubmit(values, actions, this.props)}
                >

                    {({ values, handleSubmit, isSubmitting }) => (

                        <Form onSubmit={handleSubmit}>

                            <div>
                                <label>Marca:</label>
                                <Field type="text" name="marca" />
                                <ErrorMessage name="marca" component="div" />
                            </div>

                            <div>
                                <label>Modelo:</label>
                                <Field type="text" name="modelo" />
                                <ErrorMessage name="modelo" component="div" />
                            </div>

                            <div>
                                <label>Cor:</label>
                                <Field type="text" name="cor" />
                                <ErrorMessage name="cor" component="div" />
                            </div>

                            <div>
                                <label>Placa:</label>
                                <Field type="text" name="placa" />
                                <ErrorMessage name="placa" component="div" />
                            </div>

                            <div>
                                <label>Ano Fabricação:</label>
                                <Field type="text" name="anoFab" />
                                <ErrorMessage name="anoFab" component="div" />
                            </div>

                            <div>
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Salvar</button>
                                <Link to="/carro/list" className="btn btn-danger">Cancelar</Link>
                            </div>

                        </Form>


                    )}



                </Formik>
            </div>
        );
    }

}

export default connect(carroSelector, {salvarCarro, buscarCarro})(CarroForm);