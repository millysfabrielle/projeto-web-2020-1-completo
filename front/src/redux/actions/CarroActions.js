import CarroService from "../../services/CarroService";


export const CARRO_ACTIONS = {
    LISTAR: "CARRO_LISTAR",
    BUSCAR: "CARRO_BUSCAR",
    SALVAR: "CARRO_SALVAR",
    ALTERAR: "CARRO_ALTERAR",
    EXCLUIR: "CARRO_EXCLUIR"
}

export function excluirCarro(id){

    return function(dispatch){

        return CarroService
        .delete(id)
        .then( response => {
            dispatch({
                type: CARRO_ACTIONS.EXCLUIR,
                payload: id // passa o ID do carro que foi excluído
            });
        } )
        .catch( error => {
            console.log(error);
        });

    }

}

export function salvarCarro(carro){

    return function(dispatch) {

        return CarroService
        .save(carro)
        .then( response => {
            dispatch({
                type: CARRO_ACTIONS.SALVAR,
                payload: response.data
            });
        })
        .catch( error => {
            console.log(error);
        });

    }

}

export function alterarCarro(carro){

    return function(dispatch){

        return CarroService
        .save(carro)
        .then( response => {
            dispatch({
                type: CARRO_ACTIONS.ALTERAR,
                payload: response.data
            });
        })
        .catch( error => {
            console.log(error)
        })

    }

}

export function listarCarro(){

    // callback
    return function(dispatch) {

        return CarroService
        .findAll()
            .then( response => {
                dispatch({
                    type: CARRO_ACTIONS.LISTAR,
                    payload: response.data
                });
            })
            .catch( error => {
                console.log(error);
            });
    }

}

export function buscarCarro(id){
    return function(dispatch){
        return CarroService
        .findById(id)
        .then( response => {
            dispatch({
                type: CARRO_ACTIONS.BUSCAR,
                payload: response.data
            });
        })
        .catch( error => {
            console.log(error);
        });
    }
}

export function buscarCarroPlaca(placa){

    return function(dispatch){
        return dispatch({
            type: CARRO_ACTIONS.BUSCAR,
            payload: {}
        });
    }
}