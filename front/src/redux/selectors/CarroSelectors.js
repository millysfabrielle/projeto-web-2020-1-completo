import values from "lodash/values";

export const carroListSelector = ({carroState}) => ({ carroLista: values(carroState.carro_lista) });

export const carroSelector = ({carroState}) => ({ carro: carroState.carro });