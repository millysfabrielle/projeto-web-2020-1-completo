import {CARRO_ACTIONS} from "../actions/CarroActions";


const carroState = {
    carro_lista: [],
    carro: {}
}

export default function CarroReducer(state = carroState, action){

    switch(action.type) {

        case CARRO_ACTIONS.LISTAR:
            return {
                ...state,
                carro_lista: action.payload
            }
        
        case CARRO_ACTIONS.BUSCAR:
            return {
                ...state,
                carro: action.payload
            }

        case CARRO_ACTIONS.SALVAR: 
            return {
                ...state,
                carro: {}
            }

        case CARRO_ACTIONS.ALTERAR: 
            return {
                ...state,
                carro: {}
            }

        case CARRO_ACTIONS.EXCLUIR: 
            
            return {
                ...state,
                carro_lista: state.carro_lista.filter( obj => obj.id != action.payload)
            }

        default: 
            return state;


    }


}