import axios from "axios";

class CarroService{

    constructor(){
        this.connection = axios.create({baseURL: 'http://localhost:8080'});
    }

    findAll(){
        return this.connection.get('/carro');
    }

    findById(id){
        return this.connection.get('/carro/'+id);
    }

    save( carro ){

        if(carro.id){
            return this.connection.put('/carro/', carro);
        } 

        return this.connection.post('/carro', carro);

    }

    delete( id ){
        return this.connection.delete('/carro/'+id);
    }

}

export default new CarroService();