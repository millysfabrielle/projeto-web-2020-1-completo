package br.com.arquiteturasoft.core;

import br.com.arquiteturasoft.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public class AbstractService<T> implements IService<T> {

    @Autowired
    JpaRepository<T, Long> repository;

    @Override
    public List<T> findAll() {
        List<T> lista = repository.findAll();
        return lista;
    }

    @Override
    public T findById(long id) {
        T obj = repository.getOne(id);
        return obj;
    }

    @Override
    public T save(T obj) {
        return repository.save(obj);
    }

    @Override
    public T update(T obj) {
        return repository.save(obj);
    }

    @Override
    public boolean delete(int id) {
        T obj = this.findById(id);
        if(obj != null){
            repository.delete(obj);
            return true;
        }
        return  false;
    }
}
