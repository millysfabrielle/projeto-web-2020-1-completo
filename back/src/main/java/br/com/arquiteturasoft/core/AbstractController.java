package br.com.arquiteturasoft.core;

import br.com.arquiteturasoft.entity.Pessoa;
import br.com.arquiteturasoft.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public class AbstractController<T> {

    @Autowired
    protected IService<T> service;


    @GetMapping
    public ResponseEntity<?> listAll(){
        List<T> lista = service.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?>  findById( @PathVariable long id){
        T obj = service.findById(id);

        HttpStatus status;
        if(obj != null){
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(obj, status);
    }

    @PostMapping
    public ResponseEntity<?> save( @RequestBody T obj ){

        T objCadastrado = service.save(obj);

        if(objCadastrado != null){
            return new ResponseEntity<>(objCadastrado, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping
    public ResponseEntity<?> update(  @RequestBody T obj){
        T objAlterado = service.update(obj);

        if(objAlterado != null){
            return new ResponseEntity<>(objAlterado, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id){

        boolean sucesso = service.delete(id);

        if(sucesso){
            return new ResponseEntity<>(null, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }


}
