package br.com.arquiteturasoft.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "carro")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Carro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "carro_id_seq")
    @SequenceGenerator(name = "carro_id_seq", sequenceName = "carro_id_seq", allocationSize = 1)
    @Column(name = "id")
    private long id;

    @Column(name="cor")
    private String cor;

    @Column(name="modelo")
    private String modelo;

    @Column(name="marca")
    private String marca;

    @Column(name="placa")
    private String placa;

    @Column(name="anoFab")
    private String anoFab;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getAnoFab() {
        return anoFab;
    }

    public void setAnoFab(String anoFab) {
        this.anoFab = anoFab;
    }
}
