package br.com.arquiteturasoft.service;

import br.com.arquiteturasoft.core.AbstractService;
import br.com.arquiteturasoft.entity.Carro;
import org.springframework.stereotype.Service;

@Service
public class CarroService extends AbstractService<Carro> {
}
