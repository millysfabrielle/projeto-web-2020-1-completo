package br.com.arquiteturasoft.controller;

import br.com.arquiteturasoft.core.AbstractController;
import br.com.arquiteturasoft.entity.Pessoa;
import br.com.arquiteturasoft.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pessoa")
public class PessoaController extends AbstractController<Pessoa> {}
