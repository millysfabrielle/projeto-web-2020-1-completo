package br.com.arquiteturasoft.controller;

import br.com.arquiteturasoft.core.AbstractController;
import br.com.arquiteturasoft.entity.Carro;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carro")
public class CarroController extends AbstractController<Carro> {}
